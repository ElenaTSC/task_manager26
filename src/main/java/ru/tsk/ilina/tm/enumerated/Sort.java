package ru.tsk.ilina.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.comparator.ComparatorByCreated;
import ru.tsk.ilina.tm.comparator.ComparatorByName;
import ru.tsk.ilina.tm.comparator.ComparatorByStartDate;
import ru.tsk.ilina.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

@AllArgsConstructor
@Getter
public enum Sort {

    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    CREATE_DATE("Sort by create date", ComparatorByCreated.getInstance());

    private final String displayName;
    private final Comparator comparator;

}
