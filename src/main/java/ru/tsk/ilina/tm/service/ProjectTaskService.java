package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.repository.IProjectRepository;
import ru.tsk.ilina.tm.api.repository.ITaskRepository;
import ru.tsk.ilina.tm.api.service.IProjectTaskService;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyNameException;
import ru.tsk.ilina.tm.exception.empty.EmptyUserIdException;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        if (projectRepository.existsById(userId, projectId) && taskRepository.existsById(userId, taskId))
            return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        return null;
    }

    @Override
    public Task unbindTaskById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        return taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByID(userId, projectId);
    }

}
