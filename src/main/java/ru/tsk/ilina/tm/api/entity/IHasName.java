package ru.tsk.ilina.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
